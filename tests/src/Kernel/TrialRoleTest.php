<?php

namespace Drupal\Tests\trial_role\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the basic functionality of the module.
 *
 * @group trial_role
 */
class TrialRoleTest extends KernelTestBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'trial_role',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installSchema('system', [
      'sequences',
    ]);

    // We need this across tests.
    $this->entityTypeManager = $this->container->get('entity_type.manager');
  }

  /**
   * Test that our role is added.
   */
  public function testTrialRoleAdded() {
    $this->createAndAssertUser();
  }

  /**
   * Test that the trial role is removed when we specify.
   */
  public function testTrialRoleRemoved() {
    // Set the trial role setting to 1 days.
    $this->config('trial_role.settings')->set('days', 1)->save();
    $user = $this->createAndAssertUser();
    // Make the user 3 days old.
    $user->set('created', time() - (3 * (3600 * 24)));
    $user->save();
    // Run cron.
    /** @var \Drupal\Core\CronInterface $cron */
    $cron = $this->container->get('cron');
    $cron->run();
    // Re-load the user.
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager->getStorage('user')->load($user->id());
    $this->assertFalse(in_array('trial', $user->getRoles()));
  }

  /**
   * Helper to create a user and make sure it gets the trial role added.
   *
   * @return \Drupal\user\UserInterface
   *   The newly created and saved and verified user.
   */
  protected function createAndAssertUser() {
    /** @var \Drupal\user\UserInterface $new_user */
    $new_user = $this->entityTypeManager->getStorage('user')
      ->create([
        'status' => 1,
        'name' => 'testuser@example.com',
        'mail' => 'testuser@example.com',
      ]);
    $new_user->save();
    $this->assertTrue(in_array('trial', $new_user->getRoles()));
    return $new_user;
  }

}
