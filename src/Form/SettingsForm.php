<?php

namespace Drupal\trial_role\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for this module.
 *
 * @package Drupal\trial_role\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'trial_role.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('trial_role.settings');
    $form['days'] = [
      '#type' => 'number',
      '#title' => $this->t('Trial role expiration, in days'),
      '#description' => $this->t('Enter the number of days a user should have the trial role.'),
      '#default_value' => $config->get('days'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('trial_role.settings')
      ->set('days', $form_state->getValue('days'))
      ->save();
  }

}
